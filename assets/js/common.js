﻿$(document).ready(function () {
    setTimeout(function() {
        $('body > .main').css({
            'min-height': ($(window).height() - $('body > .main').offset().top - $('body > .footer').outerHeight(true)) + 'px'
        });
        
        var tabheight = 0;
        $('.panel.panel-tabs .tab-pane').each(function () {
            var thisheight = $(this).innerHeight();
            tabheight = Math.max(tabheight, thisheight);
        });
        $('.panel.panel-tabs .tab-pane').css({'height':tabheight+'px'});
        
        if ($(document).scrollTop() >= ($('.footer').offset().top - $(window).height())) {
            $('.chat-wrapp').addClass('stoped');
        }
    }, 500); 
    
    $('body').on('click','.self-hide', function (e) {
         $(this).toggleClass('in');
    });
    
    $('body').on('click','.select-trigger', function (e) {
         var targetID = '#' + $(this).attr('for');
         selectOpen($(targetID));
         console.log(targetID);
         return false;
    });
    
    if ($.fn.datepicker) {
        $('.input-daterange').datepicker({
            format: "dd.mm.yyyy",
            language: "ru",
            autoclose: true,
            todayHighlight: true
        });
    }
        
    
});

$(window).resize(function () {
    var tabheight = 0;
    $('.panel.panel-tabs .tab-pane').css({'height':''});
    $('.panel.panel-tabs .tab-pane').each(function () {
        var thisheight = $(this).innerHeight();
        tabheight = Math.max(tabheight, thisheight);
    });
    $('.panel.panel-tabs .tab-pane').css({'height':tabheight+'px'});
    
    if ($(document).scrollTop() >= ($('.footer').offset().top - $(window).height())) {
        $('.chat-wrapp').addClass('stoped');
    }
});

$(window).scroll(function () {
    if ($(document).scrollTop() >= ($('.footer').offset().top - $(window).height())) {
        $('.chat-wrapp').addClass('stoped');
    }
    else {
        $('.chat-wrapp').removeClass('stoped');
    }
});

function selectOpen(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (element.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}
$(document).ready(function() {
        $(".category-mob").click(function() {
            $(".collapse-in").collapse('toggle');
        });
    });

$(document).ready(function () {
        var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30,
        loop:true,
        simulateTouch:false,
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });
});