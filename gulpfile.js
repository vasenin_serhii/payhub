var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');

var onError = function (err) {
    gutil.beep();
    console.log(err);
};

var src = {
    scss: 'assets/sass/**/*.scss',
    css: 'assets/css',
    
    html: ['*.html', 'Views/**/*.cshtml'],
    js: 'assets/js/**/*.js'
};

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function () {

    // browserSync({
    //     startPath: "/",
    //     proxy: "localhost:17725"
    // });
    
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(src.scss, ['sass']);
    gulp.watch(src.html).on('change', reload);
    gulp.watch(src.js).on('change', reload);
});
gulp.task('sassWatch', ['sass'], function() {
    gulp.watch(src.scss, ['sass']);
});
gulp.task('sass', function () {
    return gulp.src(src.scss)
               .pipe(plumber())
        .pipe(sass.sync({
            outputStyle: 'compact'
        }))
        .pipe(autoprefixer({
            browsers: ['last 50 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(src.css))
        .pipe(browserSync.stream());
});
gulp.task('autoprefix', function () {
    return gulp.src(src.css)
        .pipe(autoprefixer({
            browsers: ['last 50 versions'],
            cascade: false
        }));
});
gulp.task('default', ['serve']);
